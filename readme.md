# TD: Jeu du plus ou du moins basique

** Faire npm start pour lancer le serveur **

POST /party => lance une nouvelle partie, dans le body, on doit pouvoir passer un chiffre

PUT /party/current => envoi dans le body d'un chiffre, réception d'un + ou d'un - ou d'un Félicitation, le chiffre était XXX

GET /party/current => affiche l'historique de la partie en cours

GET /scores => affiche les 10 meilleurs scores
