exports.reqOn = async (req) => {
  let data = ''
    await req.on('data', chunk => {
      data = exports.parseBody(chunk)
    })
  return data
}

exports.parseBody = (c) => {
  let body = ''
  body += c.toString()
  body = JSON.parse(body)
  return body
}

exports.serverAnswer = (str, res) => {
  res.write(str)
  res.end()
}