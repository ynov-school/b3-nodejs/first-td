module.exports = class Party {
  id = null
  number = null
  pseudo = null
  count = 0
  isWinner = false

  constructor(number, pseudo) {
    this.pseudo = pseudo
    this.number = number;
  }
  
  get number() {
    return this.number
  }

  get pseudo() {
    return this.pseudo
  }

  get playerNumber() {
    return this.playerNumber
  }
 
  get count() {
    return this.count
  }

  get isWinner() {
    return this.isWinner
  }

}