const Game = require('./game')
const Party = require('./party')
const utils = require('./utils')

const http = require("http")
const host = 'localhost'
const port = 8000

let game = new Game()
let currentParty = null
let currentHistory = []

const requestListener =  async (req, res) => {
  if (req.url === '/party' && req.method === 'POST') {
    let data = await utils.reqOn(req)
    req.on('end', () => {
      let newParty = new Party(data.number, data.pseudo)
      game.addNewParty(newParty)
      currentParty = game.getCurrentParty()
      utils.serverAnswer(('Le nombre a bien était enregistré, la partie peut commencer !'), res)
    })
  }
  
  if (req.url === '/party/current' && req.method === 'PUT') {
    if (currentParty && !currentParty.isWinner) {
      let data = await utils.reqOn(req)
      let message = null
      
      req.on('end', () => {
        if (data.number == currentParty.number) { 
          message = `Félicitation le chiffre était bien ${currentParty.number}`
          game.updatePartyCount()
          game.addNewScore(currentParty.count, currentParty.pseudo)
          currentParty.isWinner = true
          currentHistory = []
        } else {
          data.number > currentParty.number ? message = '-' : message = '+' 
          let attempt = {
            number: data.number,
            message: message
          }
          currentHistory.push(attempt)
        }
        game.updatePartyCount()
        utils.serverAnswer((message), res)
      })
    } else {
      res.error
    } 
  }

  if (req.url === '/party/current' && req.method === 'GET') {
    const currentHistoryToDisplay = JSON.stringify(currentHistory)
    utils.serverAnswer((`${currentHistoryToDisplay}`), res) 
  }

  if (req.url === '/scores' && req.method === 'GET') {
    utils.serverAnswer((`${game.bestScores()}`), res)
  }
}

const server = http.createServer(requestListener)

server.listen(port, host, () => {
  console.log(`Server is running on http://${host}:${port}`)
})
