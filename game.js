module.exports = class Game {
  parties = [];
  scores = [];

  get parties() {
    return this.parties
  }

  get bestScores() {
    return this.getThe10BestScores();
  }

  addNewParty(party) {
    let newParty = party
    newParty.id = this.parties.length > 0 ? this.parties[this.parties.length - 1].id + 1 : 0
    this.parties.push(newParty) 
  }

  addNewScore(score, pseudo) {
    let newScore = {
      score: score,
      pseudo: pseudo
    }
    this.scores.push(newScore)
  }

  getCurrentParty() {
    return this.parties[this.parties.length - 1]
  }

  updatePartyCount() {
    this.parties[this.parties.length - 1].count++
  }

  getTheTenBestScores() {
    let tenBestScores = this.scores.sort((a, b) => a.score - b.score).slice(0, 10)
    return JSON.stringify(tenBestScores)
  }
}