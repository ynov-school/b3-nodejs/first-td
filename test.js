// BUBBLE SORTING vs js sort function (just experience to practice algo)

let inputArr = [ { score: 2, pseudo: 'dudu' }, { score: 1, pseudo: 'dudu' }, { score: 3, pseudo: 'dudu' }, { score: 2, pseudo: 'dudu' }, { score: 1, pseudo: 'dudu' }, { score: 3, pseudo: 'dudu' }, { score: 2, pseudo: 'dudu' }, { score: 1, pseudo: 'dudu' }, { score: 3, pseudo: 'dudu' }, { score: 2, pseudo: 'dudu' }, { score: 1, pseudo: 'dudu' }, { score: 3, pseudo: 'dudu' },{ score: 2, pseudo: 'dudu' }, { score: 1, pseudo: 'dudu' }, { score: 3, pseudo: 'dudu' } ]

const t = (inputArr) => {
  let len = inputArr.length;
  for (let i = 0; i < len; i++) {
    for (let j = 0; j < len; j++) {
      if (inputArr[j + 1] && inputArr[j].score > inputArr[j + 1].score ) {
        let tmp = inputArr[j];
        inputArr[j] = inputArr[j + 1];
        inputArr[j + 1] = tmp;
      }
    }
  }
  return inputArr;
};

console.log(t(inputArr));

const z = (arr) => {
  return arr.sort((a, b) => a.score - b.score)
}

console.log(z(inputArr));